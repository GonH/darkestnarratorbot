const config = require("./config");
const TeleBot = require("telebot");
const {
  BOT_TOKEN
} = config;
const {
  tellAnythingHandler,
  tellHandler,
  tellByGroupHandler,
  tellByTypeHandler,
  tellBySituationHandler,
  sayHandler,
  sayAnythingHandler,
  sayByGroupHandler,
  sayByTypeHandler,
  sayBySituationHandler,
} = require("./src/handlers");
const bot = new TeleBot(BOT_TOKEN);

bot.on("/writeAnything", tellAnythingHandler);

bot.on("/tell", tellHandler);

bot.on("/tellByGroup", tellByGroupHandler);

bot.on("/tellByType", tellByTypeHandler);

bot.on("/tellBySituation", tellBySituationHandler);

bot.on("/sayAnything", sayAnythingHandler)

bot.on("/say", sayHandler);

bot.on("/sayByGroup", sayByGroupHandler);

bot.on("/sayByType", sayByTypeHandler);

bot.on("/sayBySituation", sayBySituationHandler);

bot.start();