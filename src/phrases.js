const constants = require("./constants");
const {
	group: {
		hamlet,
		intro,
		heroes,
		items,
		ancestor,
		dungeon,
		quest,
		fight,
		torch
	},
	type: {
		misc,
		downpushing,
		bad,
		good,
		uplift,
		middle,
		horror,
		epic,
		smug,
		horrible,
		TOPTIER
	},
	situation: {
		introduction,
		firstHamlet,
		enterHamlet,
		explanation,
		stageCoach,
		tavern,
		abby,
		blacksmith,
		guild,
		survivalist,
		nomadWagon,
		sanitarium,
		recluting,
		dismissing,
		ruins,
		enterBossQuest,
		weald,
		warrens,
		cove,
		afterBossQuest,
		expFail,
		bossFight,
		torch100,
		torch0,
		camping,
		trapHit,
		obstacle,
		hunger,
		heal,
		kill,
		weakKill,
		strongKill,
		bigKill,
		blightOrBleed,
		hit,
		beingHit,
		beingDebuffed,
		beingHorrorized,
		beingBuffed,
		deathDoor,
		deathBlow,
		win,
		fullInventory,
		escapeFail,
		escape,
		stressAt50,
		afflicted,
		virtuous
	}
} = constants;


module.exports = {
	phrases: [{
			group: hamlet,
			type: misc,
			situation: introduction,
			audio: `theHamletIsJustAhead`,
			phrase: `Brigands have the run of these lanes. Keep to the side path; the Hamlet is just ahead.`
		},
		{
			group: intro,
			type: misc,
			situation: introduction,
			audio: `dispatchTheseThugsInBrutalFashion`,
			phrase: `Dispatch this thug in brutal fashion, that all may hear of your arrival!`
		},
		{
			group: intro,
			type: misc,
			situation: introduction,
			audio: `leaveNothingUnchecked`,
			phrase: `Leave nothing unchecked, there is much to be found in forgotten places.`
		},
		{
			group: intro,
			type: misc,
			situation: introduction,
			audio: `theRightfullOwnerHasReturned`,
			phrase: `An ambush! Send these vermin a message: the rightful owner has returned, and their kind is no longer welcome.`
		},
		{
			group: hamlet,
			type: misc,
			situation: introduction,
			audio: `welcomeHomeSuchAsItIs`,
			phrase: `Welcome home, such as it is. This squalid hamlet, these corrupted lands, they are yours now, and you are bound to them.`
		},
		{
			group: hamlet,
			type: misc,
			situation: firstHamlet,
			audio: `aMeccaOfMadness`,
			phrase: `This sprawling estate, a Mecca of madness and morbidity. Your work begins...`
		},
		{
			group: hamlet,
			type: downpushing,
			situation: firstHamlet,
			audio: `theCostOfPreparadness`,
			phrase: `The cost of preparedness - measured now in gold, later in blood.`
		},
		{
			group: heroes,
			type: misc,
			situation: firstHamlet,
			audio: `foolsAndCorpses`,
			phrase: `Women and men; soldiers and outlaws; fools and corpses. All will find their way to us now that the road is clear.`
		},
		{
			group: hamlet,
			type: misc,
			situation: firstHamlet,
			audio: `andTheBrokenAlike`,
			phrase: `Fresh kegs, cards, and curtained rooms promise solace to the weary and the broken alike.`
		},
		{
			group: hamlet,
			type: misc,
			situation: firstHamlet,
			audio: `theAbbeyCallsToTheFaithfull`,
			phrase: `The cobwebs have been dusted, the pews set straight. The Abbey calls to the faithful...`
		},
		{
			group: hamlet,
			type: misc,
			situation: firstHamlet,
			audio: `theForgeStandsReadyToMakeWeaponsOfWar`,
			phrase: `The bellows blast once again! The forge stands ready to make weapons of war.`
		},
		{
			group: hamlet,
			type: downpushing,
			situation: firstHamlet,
			audio: `weWillFaceEverGreaterThreats`,
			phrase: `Make no mistake, we will face ever greater threats. Our soldiers must be ready.`
		},
		{
			group: hamlet,
			type: misc,
			situation: firstHamlet,
			audio: `aStrictInstructor`,
			phrase: `At home in wild places, she is a stalwart survivor, and a strict instructor.`
		},
		{
			group: items,
			type: misc,
			situation: firstHamlet,
			audio: `trincketsAndCharms`,
			phrase: `Trinkets and charms, gathered from all the forgotten corners of the earth...`
		},
		{
			group: hamlet,
			type: misc,
			situation: firstHamlet,
			audio: `mostEndUpHere`,
			phrase: `Most will end up here, covered in the poisoned earth, awaiting merciful oblivion.`
		},
		{
			group: ancestor,
			type: bad,
			situation: enterHamlet,
			audio: `inTimeYouWillKnowTheTragincExtentOfMyFailings`,
			phrase: `In time, you will know the tragic extent of my failings...`
		},
		{
			group: hamlet,
			type: misc,
			situation: enterHamlet,
			audio: `iRememberWhenTheSunShone`,
			phrase: `I remember days when the sun shone, and laughter could be heard from the tavern.`
		},
		{
			group: ancestor,
			type: bad,
			situation: enterHamlet,
			audio: `iWasLordOfThisPlace`,
			phrase: `I was lord of this place, before the crows and rats made it their domain.`
		},
		{
			group: ancestor,
			type: misc,
			situation: enterHamlet,
			audio: `inTruthICannotTell`,
			phrase: `In truth I cannot tell how much time has passed since I sent that letter.`
		},
		{
			group: hamlet,
			type: misc,
			situation: enterHamlet,
			audio: `envyOfThisLand`,
			phrase: `Once, our estate was the envy of this land...`
		},
		{
			group: ancestor,
			type: misc,
			situation: enterHamlet,
			audio: `barelyWhisperedAloudByDecentFolk`,
			phrase: `Our family name, once so well regarded, is now barely whispered aloud by decent folk.`
		},
		{
			group: ancestor,
			type: good,
			situation: enterHamlet,
			audio: `glimmerOfHope`,
			phrase: `I see something long-absent in the sunken faces of passersby - a glimmer of hope.`
		},
		{
			group: hamlet,
			type: misc,
			situation: enterHamlet,
			audio: `thePoorCaretaker`,
			phrase: `The poor Caretaker, I fear his long-standing duties here have ...affected him.`
		},
		{
			group: hamlet,
			type: bad,
			situation: enterHamlet,
			audio: `theDegeneracyOfTheHamlet`,
			phrase: `The degeneracy of the Hamlet is nothing, I fear, when compared to the condition of surrounding acres.`
		},
		{
			group: ancestor,
			type: bad,
			situation: explanation,
			audio: `myObsessionCaused`,
			phrase: `My obsession caused this great foulness, and it is shameful that I must rely upon you to set it right.`
		},
		{
			group: ancestor,
			type: misc,
			situation: explanation,
			audio: `iWasDeadBeforeTheyFoundMe`,
			phrase: `I can still see their angry faces as they stormed the manor, but I was dead before they found me, and the letter was on its way.`
		},
		{
			group: dungeon,
			type: bad,
			situation: explanation,
			audio: `thereIsAGreatHorrorBeneathThisMannor`,
			phrase: `There is a great horror beneath the manor: a Crawling Chaos that must be destroyed!`
		},
		{
			group: ancestor,
			type: bad,
			situation: explanation,
			audio: `curiosityInterestAndObsession`,
			phrase: `Curiosity, interest, and obsession — mile markers on my road to damnation.`
		},
		{
			group: quest,
			type: misc,
			situation: explanation,
			audio: `theCostOfThisCrusade`,
			phrase: `Trouble yourself not with the cost of this crusade - its noble end affords you broad tolerance in your choice of means.`
		},
		{
			group: ancestor,
			type: misc,
			situation: explanation,
			audio: `theTerribleWondersIHaveComeToKnow`,
			phrase: `Let me share with you the terrible wonders I have come to know...`
		},
		{
			group: ancestor,
			type: bad,
			situation: introduction,
			audio: `youAnsweredTheLeter`,
			phrase: `You answered the letter — now like me, you are part of this place.`
		},
		{
			group: ancestor,
			type: misc,
			situation: explanation,
			audio: `weDugForMonths`,
			phrase: `We dug for months, years — an eternity. And we were rewarded with madness.`
		},
		{
			group: ancestor,
			type: bad,
			situation: explanation,
			audio: `thePlumeAndThePistol`,
			phrase: `The plume and the pistol — a fitting end to my folly, and a curse upon us all.`
		},
		{
			group: dungeon,
			type: bad,
			situation: explanation,
			audio: `teniuslyThin`,
			phrase: `Can you feel it? The walls between the sane world and that unplumbed dimension of delirium are tenuously thin here...`
		},
		{
			group: ancestor,
			type: misc,
			situation: explanation,
			audio: `gnawingAtTheBackOfMyMind`,
			phrase: `All my life, I could feel an insistent gnawing in the back of my mind. It was a yearning, a thirst for discovery that could be neither numbed, nor sated.`
		},
		{
			group: ancestor,
			type: misc,
			situation: explanation,
			audio: `eternityOfFutileStruggling`,
			phrase: `An eternity of futile struggle — a penance for my unspeakable transgressions.`
		},
		{
			group: ancestor,
			type: bad,
			situation: explanation,
			audio: `finalCrowningThing`,
			phrase: `All the decadent horrors I have seen pale in comparison with that final, crowning thing. I could not look, nor could I look away!`
		},
		{
			group: heroes,
			type: uplift,
			situation: stageCoach,
			audio: `greatHeroes`,
			phrase: `Great heroes can be found even here, in the mud and rain.`
		},
		{
			group: heroes,
			type: bad,
			situation: stageCoach,
			audio: `foolishlySeekingFortuneAndGlory`,
			phrase: `More arrive, foolishly seeking fortune and glory in this domain of the damned.`
		},
		{
			group: heroes,
			type: bad,
			situation: stageCoach,
			audio: `wordIsTravelling`,
			phrase: `Word is travelling. Ambition is stirring in distant cities. We can use this.`
		},
		{
			group: hamlet,
			type: misc,
			situation: tavern,
			audio: `withEnoughAle`,
			phrase: `With enough ale, maybe they can be inured against the horrors below.`
		},
		{
			group: hamlet,
			type: misc,
			situation: tavern,
			audio: `theTreshholdWithCoin`,
			phrase: `All manner of diversion and dalliance await those who cross the threshold with coin in hand.`
		},
		{
			group: hamlet,
			type: misc,
			situation: tavern,
			audio: `theRushOfLife`,
			phrase: `Strong drink, a game of chance, and companionship. The rush of life.`
		},
		{
			group: hamlet,
			type: misc,
			situation: abby,
			audio: `aLittleHope`,
			phrase: `A little hope, however desperate, is never without worth.`
		},
		{
			group: hamlet,
			type: misc,
			situation: abby,
			audio: `dogmaticRituals`,
			phrase: `Gilded icons and dogmatic rituals... for some, a tonic against the bloodshed.`
		},
		{
			group: hamlet,
			type: bad,
			situation: abby,
			audio: `aManInARobe`,
			phrase: `A man in a robe, claiming communion with the divine. Madness.`
		},
		{
			group: hamlet,
			type: misc,
			situation: blacksmith,
			audio: `aStrongArmAndTemperedSteel`,
			phrase: `In the end, every plan relies upon a strong arm, and tempered steel.`
		},
		{
			group: hamlet,
			type: misc,
			situation: blacksmith,
			audio: `anythingToProlongASoldiersLife`,
			phrase: `A sharper sword, a stronger shield. Anything to prolong a soldier's life.`
		},
		{
			group: hamlet,
			type: misc,
			situation: blacksmith,
			audio: `fanTheFlames`,
			phrase: `Fan the flames! Mold the metal! We are raising an army!`
		},
		{
			group: hamlet,
			type: good,
			situation: guild,
			audio: `theirKnoweldegeLivesOn`,
			phrase: `Some may fall, but their knowledge lives on.`
		},
		{
			group: hamlet,
			type: good,
			situation: guild,
			audio: `theWiseHeroTrains`,
			phrase: `Every creature has a weakness. The wise hero trains for what she will face.`
		},
		{
			group: hamlet,
			type: misc,
			situation: guild,
			audio: `aStrictRegimen`,
			phrase: `A strict regimen is paramount, if one is to master the brutal arithmetic of combat.`
		},
		{
			group: hamlet,
			type: misc,
			situation: survivalist,
			audio: `survivalIsTheSame`,
			phrase: `Alone in the woods or tunnels, survival is the same. Prepare, persist, and overcome.`
		},
		{
			group: hamlet,
			type: misc,
			situation: survivalist,
			audio: `successDependsOnSurvival`,
			phrase: `Success depends on survival.`
		},
		{
			group: hamlet,
			type: misc,
			situation: survivalist,
			audio: `moreThanBrutalBloodletting`,
			phrase: `They must learn more than brutal bloodletting — they must learn to survive!`
		},
		{
			group: items,
			type: misc,
			when: nomadWagon,
			audio: `soldAtAProfitOfCourse`,
			phrase: `Rarities and curios, sold at a profit, of course.`
		},
		{
			group: items,
			type: misc,
			when: nomadWagon,
			audio: `thesimplestObjectCanBeATalismanAgainstEvil`,
			phrase: `Idol, amulet, or lucky charm — the simplest object can be a talisman against evil.`
		},
		{
			group: items,
			type: misc,
			when: nomadWagon,
			audio: `gatheredFromForbiddenPlaces`,
			phrase: `An increasing stockpile of curious trinkets, gathered from forbidden places.`
		},
		{
			group: hamlet,
			type: bad,
			situation: sanitarium,
			audio: `theFrontlineOfThisWar`,
			phrase: `The front line of this war is not in the dungeon, but rather, inside the mind.`
		},
		{
			group: hamlet,
			type: misc,
			situation: sanitarium,
			audio: `experimental`,
			phrase: `Experimental techniques and tonics can overcome things a sharpened sword cannot.`
		},
		{
			group: hamlet,
			type: misc,
			situation: sanitarium,
			audio: `calmTheMostTormentedSoul`,
			phrase: `Curious methodologies and apparatus can calm even the most tormented soul.`
		},
		{
			group: heroes,
			type: bad,
			situation: recluting,
			audio: `moreDangerousThanHeSeems`,
			phrase: `Tortured and reclusive... this man is more dangerous than he seems...`
		},
		{
			group: heroes,
			type: misc,
			situation: recluting,
			audio: `sheSearchesWhereOthersWillNotGo`,
			phrase: `She searches where others will not go... and sees what others will not see.`
		},
		{
			group: heroes,
			type: misc,
			situation: recluting,
			audio: `shootBandage`,
			phrase: `Shoot, bandage and pillage: the dancing steps of war.`
		},
		{
			group: heroes,
			type: misc,
			situation: recluting,
			audio: `theThrillOfTheHunt`,
			phrase: `The thrill of the hunt... The promise of payment...`
		},
		{
			group: heroes,
			type: good,
			situation: recluting,
			audio: `swordArmAnchoredByHolyPurpouse`,
			phrase: `A mighty sword-hand anchored by a holy purpose. A zealous warrior.`
		},
		{
			group: heroes,
			type: misc,
			situation: recluting,
			audio: `daggersPoint`,
			phrase: `To those with the keen eye, gold gleams like a dagger's point.`
		},
		{
			group: heroes,
			type: misc,
			situation: recluting,
			audio: `unrelentingSavagery`,
			phrase: `Barbaric rage and unrelenting savagery make for a powerful ally.`
		},
		{
			group: heroes,
			type: good,
			situation: recluting,
			audio: `elusiveEvasivePersistent`,
			phrase: `Elusive, evasive, persistent. Righteous traits for a rogue.`
		},
		{
			group: heroes,
			type: misc,
			situation: recluting,
			audio: `hisFaithfullBeast`,
			phrase: `A lawman and his faithful beast. A bond forged by battle and bloodshed.`
		},
		{
			group: heroes,
			type: good,
			situation: recluting,
			audio: `laughingStillAtTheEnd`,
			phrase: `He will be laughing still... at the end.`
		},
		{
			group: heroes,
			type: bad,
			situation: recluting,
			audio: `adversityAndExcist`,
			phrase: `This man understands that adversity and existence are one and the same.`
		},
		{
			group: heroes,
			type: misc,
			situation: recluting,
			audio: `hundredCampaigns`,
			phrase: `The raw strength of youth may be spent, but his eyes hold the secrets of a hundred campaigns.`
		},
		{
			group: heroes,
			type: bad,
			situation: recluting,
			audio: `toFightTheAbyssOneMustKnowIt`,
			phrase: `To fight the abyss, one must know it...`
		},
		{
			group: heroes,
			type: misc,
			situation: recluting,
			audio: `whatBetterLaboratory`,
			phrase: `What better laboratory than the blood-soaked battlefield?`
		},
		{
			group: heroes,
			type: good,
			situation: recluting,
			audio: `piousAndUnrelenting`,
			phrase: `A sister of battle. Pious and unrelenting.`
		},
		{
			group: heroes,
			type: misc,
			situation: recluting,
			audio: `championMarkswoman`,
			phrase: `A champion markswoman keen for a new kind of challenge.`
		},
		{
			group: heroes,
			type: bad,
			situation: dismissing,
			audio: `hasBecomeVestigial`,
			phrase: `This one has become vestigial, useless.`
		},
		{
			group: heroes,
			type: bad,
			situation: dismissing,
			audio: `sufferNotTheLameHorse`,
			phrase: `Suffer not the lame horse... nor the broken man.`
		},
		{
			group: heroes,
			type: bad,
			situation: dismissing,
			audio: `anotherSoulBatheredAndBroken`,
			phrase: `Another soul battered and broken, cast aside like a spent torch.`
		},
		{
			group: heroes,
			type: bad,
			situation: dismissing,
			audio: `mustMoveOn`,
			phrase: `Those without the stomach for this place must move on.`
		},
		{
			group: heroes,
			type: bad,
			situation: dismissing,
			audio: `theConditionOfThosePoorDevilsThatRemain`,
			phrase: `It is done. Turn yourself now to the conditions of those poor devils who remain.`
		},
		{
			group: heroes,
			type: bad,
			situation: dismissing,
			audio: `sendThisOneToJourneyElsewhere`,
			phrase: `Send this one to journey elsewhere, for we have need of sterner stock.`
		},
		{
			group: heroes,
			type: bad,
			situation: dismissing,
			audio: `thisOneIsNoMoreGoodUseToUs`,
			phrase: `Slumped shoulders, wild eyes, and a stumbling gait - this one is no more good to us.`
		},
		{
			group: heroes,
			type: bad,
			situation: dismissing,
			audio: `thePathAheadIsTerrible`,
			phrase: `The task ahead is terrible, and weakness cannot be tolerated.`
		},
		{
			group: dungeon,
			type: misc,
			situation: ruins,
			audio: `theHallsOfYourLineage`,
			phrase: `Pace out the halls of your lineage, once familiar, now foreign.`
		},
		{
			group: dungeon,
			type: misc,
			situation: ruins,
			audio: `mustBeDrivenBack`,
			phrase: `The fiends must be driven back, and what better place to begin than the seat of our noble line?`
		},
		{
			group: dungeon,
			type: misc,
			situation: ruins,
			audio: `canAFallenOneFindRest`,
			phrase: `Can the defiled be consecrated? Can the fallen find rest?`
		},
		{
			group: dungeon,
			type: misc,
			situation: ruins,
			audio: `thereIsPowerInSymbols`,
			phrase: `There is power in symbols. Collect the scattered scraps of faith and give comfort to the masses.`
		},
		{
			group: dungeon,
			type: misc,
			situation: enterBossQuest,
			audio: ``,
			phrase: `A devil walks these halls... only the mad or the desperate go in search of him.`
		},
		{
			group: dungeon,
			type: misc,
			situation: enterBossQuest,
			audio: `theEchoesOfHis`,
			phrase: `The echoes of his mindless tittering reverberate maddeningly...`
		},
		{
			group: dungeon,
			type: misc,
			when: weald,
			audio: `iKnewAllThesePath`,
			phrase: `I knew all these paths once; now they are as twisted as my own ambitions.`
		},
		{
			group: dungeon,
			type: bad,
			when: weald,
			audio: `corruptionHasSoakedTheSoil`,
			phrase: `Corruption has soaked the soil, sapping all good life from these groves - let us burn out this evil.`
		},
		{
			group: dungeon,
			type: misc,
			when: weald,
			audio: `landMayYetLive`,
			phrase: `Excise the fungal tumors and the land may yet live.`
		},
		{
			group: dungeon,
			type: misc,
			when: weald,
			audio: `everyLostResourceMustBeRecovered`,
			phrase: `Our land is remote and unneighbored. Every lost resource must be recovered.`
		},
		{
			group: dungeon,
			type: misc,
			when: enterBossQuest,
			audio: `thereIsMethod`,
			phrase: `There is method in the wild corruption here. It bears a form both wretched and malevolent.`
		},
		{
			group: dungeon,
			type: misc,
			when: enterBossQuest,
			audio: `theSmellOfSulphur`,
			phrase: `The smell of sulfur and gunpowder hangs in the air, the war machine is close.`
		},
		{
			group: dungeon,
			type: misc,
			when: warrens,
			audio: `weMustScoutTheirSqualidHomes`,
			phrase: `To prosecute our war against the Swine, we must first scout their squalid homes.`
		},
		{
			group: dungeon,
			type: bad,
			when: warrens,
			audio: `theyBreedQuicklyDownInTheDark`,
			phrase: `They breed quickly down there in the dark, but perhaps we can slay them even faster.`
		},
		{
			group: dungeon,
			type: misc,
			when: warrens,
			audio: `tearThemDown`,
			phrase: `The Swine draw power from their horrid markings and crude idols - tear them down!`
		},
		{
			group: dungeon,
			type: misc,
			when: warrens,
			audio: `stealTheirFood`,
			phrase: `Even the fiercest beast will lay down when it has not eaten. Steal their food.`
		},
		{
			group: dungeon,
			type: misc,
			when: enterBossQuest,
			audio: `namelessAbomination`,
			phrase: `A nameless abomination, a testament to my failures - it must be destroyed!`
		},
		{
			group: dungeon,
			type: misc,
			when: enterBossQuest,
			audio: `moreTerribleThanICanDescribe`,
			phrase: `The thing is more terrible than I can describe - an incoherent jumble of organ, sinew and bone.`
		},
		{
			group: dungeon,
			type: bad,
			situation: cove,
			audio: `theSmellOfRottingFish`,
			phrase: `The smell of rotting fish is almost unbearable...`
		},
		{
			group: dungeon,
			type: misc,
			situation: cove,
			audio: `theyMustBeFlushedOut`,
			phrase: `These salt-soaked caverns are teeming with pelagic nightmares - they must be flushed out!`
		},
		{
			group: dungeon,
			type: misc,
			situation: cove,
			audio: `letUsClaimThisPlaceAnew`,
			phrase: `The flopping, fish-like things abhore the warding sigils. Let us claim this place anew!`
		},
		{
			group: dungeon,
			type: misc,
			situation: cove,
			audio: `recoverTheLostShipments`,
			phrase: `Recover these lost shipments of rarities, that we may prevent them from falling into even less scrupulous hands...`
		},
		{
			group: dungeon,
			type: misc,
			situation: enterBossQuest,
			audio: `whatBecameOfTheUnfortunate`,
			phrase: `I always wondered what became of the unfortunate little waif...`
		},
		{
			group: dungeon,
			type: misc,
			situation: enterBossQuest,
			audio: `thePoorDevils`,
			phrase: `The poor devils, chained and drowning for eternity...`
		},
		{
			group: dungeon,
			type: misc,
			situation: ruins,
			audio: `shiftedCorridors`,
			phrase: `The shifted corridors and sloped vaults of our ancestry are beginning to feel familiar.`
		},
		{
			group: dungeon,
			type: misc,
			situation: ruins,
			audio: `theGreatRuinsBelongToUs`,
			phrase: `The great Ruins belong to us, and we will find whatever secrets they hold.`
		},
		{
			group: dungeon,
			type: misc,
			situation: ruins,
			audio: `returnToRest`,
			phrase: `More bones returned to rest. Devils remanded to their abyss.`
		},
		{
			group: dungeon,
			type: uplift,
			situation: ruins,
			audio: `roomByRoomHallByHall`,
			phrase: `Room by room, hall by hall, we reclaim what is ours.`
		},
		{
			group: dungeon,
			type: uplift,
			situation: ruins,
			audio: `thisDayBlongsToTheLight`,
			phrase: `This day belongs to the Light!`
		},
		{
			group: dungeon,
			type: uplift,
			situation: ruins,
			audio: `beaconsIntheDarkness`,
			phrase: `Beacons in the darkness, stars in the emptiness of the void.`
		},
		{
			group: dungeon,
			type: uplift,
			situation: ruins,
			audio: `tokensOfHome`,
			phrase: `Tokens of hope, recovered from the encroaching dark.`
		},
		{
			group: dungeon,
			type: misc,
			situation: ruins,
			audio: `hisFaithHaveBeenRestored`,
			phrase: `The Abbot will be grateful - the trappings of his faith have been restored.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `didHeForseeHisOwnDemise`,
			phrase: `Did he foresee his own demise? I care not, so long as he remains dead.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: ``,
			phrase: `In life, his claims to precognition were dubious at best, in death, they are ridiculous.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `evenTheDeadCanDieAgain`,
			phrase: `Even reanimated bones can fall; even the dead can die again.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `willTheseWalkingBonesFinnalyFail`,
			phrase: `With no living sinew to actuate them, will these walking bones finally fail?`
		},
		{
			group: dungeon,
			type: misc,
			situation: weald,
			audio: ``,
			phrase: `Every cleared path and charted route reduces the isolation of our troubled estate.`
		},
		{
			group: dungeon,
			type: misc,
			situation: weald,
			audio: ``,
			phrase: `Paths and roads bring soldiers and supplies, let them arrive unharried!`
		},
		{
			group: dungeon,
			type: misc,
			situation: weald,
			audio: ``,
			phrase: `Driving out corruption is an endless battle, but one that must be fought.`
		},
		{
			group: dungeon,
			type: misc,
			situation: weald,
			audio: ``,
			phrase: `The agents of pestilence will yet be driven from our woods!`
		},
		{
			group: dungeon,
			type: misc,
			situation: weald,
			audio: `goodFortuneAndHardWork`,
			phrase: `Good fortune and hard work may yet arrest this plague.`
		},
		{
			group: dungeon,
			type: good,
			situation: weald,
			audio: `dissinfectionAtLast`,
			phrase: `Disinfection, at last.`
		},
		{
			group: dungeon,
			type: misc,
			situation: weald,
			audio: ``,
			phrase: `These medicines will prevent the outbreak of epidemic at our struggling Hamlet.`
		},
		{
			group: dungeon,
			type: misc,
			situation: weald,
			audio: ``,
			phrase: `These tonics and herbs will stave off infection and neutralize contagion.`
		},
		{
			group: dungeon,
			type: misc,
			situation: weald,
			audio: `theWoodIsStillPoisoned`,
			phrase: `The wood is still poisoned. The way is still blocked. But less people will be eaten.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `leaveHerCorpseToRot`,
			phrase: `Leave her corpse to rot, consumed by the spores she spawned.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `aCorpseOfTwistedMetal`,
			phrase: `A corpse of twisted metal and splintered wood - at home amongst the headstones.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `theBrigandIsUndone`,
			phrase: `The Brigands are undone - our family crest is once again a symbol of strength!`
		},
		{
			group: dungeon,
			type: misc,
			situation: warrens,
			audio: ``,
			phrase: `The swinefolk's labyrinth may yet prove to be navigable.`
		},
		{
			group: dungeon,
			type: misc,
			situation: warrens,
			audio: ``,
			phrase: `The twisting tunnels seem a little less ...impossible.`
		},
		{
			group: dungeon,
			type: misc,
			situation: warrens,
			audio: ``,
			phrase: `Some experiments should have never happened. You are doing just work, ending them.`
		},
		{
			group: dungeon,
			type: misc,
			situation: warrens,
			audio: ``,
			phrase: `Their squeals fade, their confidence is shaken!`
		},
		{
			group: dungeon,
			type: misc,
			situation: warrens,
			audio: `letThoseDirtyBeastsWorshipTheMud`,
			phrase: `Ha ha ha! Let those dirty beasts worship the mud now!`
		},
		{
			group: dungeon,
			type: misc,
			situation: warrens,
			audio: `everMoreIgnorant`,
			phrase: `Robbed of their writings, the Swine will grow ever more ignorant - if such a thing were possible.`
		},
		{
			group: dungeon,
			type: misc,
			situation: warrens,
			audio: ``,
			phrase: `These foodstuffs yield double benefit: the town may eat, and the Swine will not.`
		},
		{
			group: dungeon,
			type: good,
			situation: warrens,
			audio: ``,
			phrase: `Our supplies are replenished, the soldiers will feast tonight.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `grotesqueInDeath`,
			phrase: `It is as grotesque in death as it was in life...`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `smallConsolation`,
			phrase: `Its destruction is a small consolation, given the implications of its terrible existence.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `gnawToATonOfPutridFlesh`,
			phrase: `How many rats will it take to gnaw through a tonne of putrid flesh?`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `horribleInDeath`,
			phrase: `The thing is even more horrible in death. Liquefaction cannot come soon enough.`
		},
		{
			group: dungeon,
			type: misc,
			situation: cove,
			audio: `morbidAspect`,
			phrase: `Despite its morbid aspect, this twisted, cavernous maze seems almost traversable.`
		},
		{
			group: dungeon,
			type: misc,
			situation: cove,
			audio: `wateryTomb`,
			phrase: `We will find all manner of great and terrible things in this watery tomb...`
		},
		{
			group: dungeon,
			type: misc,
			situation: cove,
			audio: `thingsAreDrivenBack`,
			phrase: `The pungent odour abates! The things are driven back, for a time.`
		},
		{
			group: dungeon,
			type: uplift,
			situation: cove,
			audio: `wholesomeMarineLife`,
			phrase: `At last, wholesome marine life can flourish - if indeed there is such a thing.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `vileQueen`,
			phrase: `Hideous matriarch, vile queen of the aphotic depths - she has no place in the sane world!`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `theRoutesAreSafe`,
			phrase: `Seafaring trade, the lifeblood of any port, can resume again now that the routes are safe.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `capptainAndCrew`,
			phrase: `Finally, a sailor's death for captain and crew. Fitting.`
		},
		{
			group: dungeon,
			type: misc,
			situation: afterBossQuest,
			audio: `cursedToFloatForever`,
			phrase: `They are cursed to float forever, deep in the swirling blackness, far beyond the light's reach.`
		},
		{
			group: quest,
			type: uplift,
			situation: expFail,
			audio: `nottheEntOfThings`,
			phrase: `A setback, but not the end of things.`
		},
		{
			group: quest,
			type: middle,
			situation: expFail,
			audio: `woundsToBeTended`,
			phrase: `Wounds to be tended; lessons to be learned.`
		},
		{
			group: quest,
			type: bad,
			situation: expFail,
			audio: `reagroupReassemble`,
			phrase: `Regroup. Reassemble. Evil is timeless, after all.`
		},
		{
			group: quest,
			type: middle,
			situation: expFail,
			audio: `hearthBraingAndBoddy`,
			phrase: `Failure tests the mettle of heart, brain, and body.`
		},
		{
			group: quest,
			type: middle,
			situation: expFail,
			audio: `weWillEndureThisLose`,
			phrase: `You will endure this loss, and learn from it.`
		},
		{
			group: quest,
			type: bad,
			situation: expFail,
			audio: `cannotLearnAThing`,
			phrase: `You cannot learn a thing you think you know...`
		},
		{
			group: quest,
			type: uplift,
			situation: expFail,
			audio: `pickOurselvesUp`,
			phrase: `We fall so that we may learn to pick ourselves up once again.`
		},
		{
			group: quest,
			type: uplift,
			situation: expFail,
			audio: `fleetingFailure`,
			phrase: `Do not ruminate on this fleeting failure - the campaign is long, and victory will come.`
		},
		{
			group: quest,
			type: middle,
			situation: expFail,
			audio: `thereIsNoPerilInTheTask`,
			phrase: `Where there is no peril in the task, there can be no glory in its accomplishment.`
		},
		{
			group: quest,
			type: bad,
			situation: expFail,
			audio: `ignoranceOfYourEnemy`,
			phrase: `Ignorance of your enemy and of yourself will invariably lead to defeat.`
		},
		{
			group: quest,
			type: uplift,
			situation: expFail,
			audio: `itIsTheFireThatTempers`,
			phrase: `Great adversity has a beauty - it is the fire that tempers the blade.`
		},
		{
			group: fight,
			type: horror,
			situation: bossFight,
			audio: `toweringFierce`,
			phrase: `Towering, fierce, terrible. Nightmare made material!`
		},
		{
			group: fight,
			type: misc,
			situation: bossFight,
			audio: `theMadman`,
			phrase: `The madman hides there, behind the pews, spouting his mindless drivel!`
		},
		{
			group: fight,
			type: horror,
			situation: bossFight,
			audio: `testimentToThePowersOfCorruption`,
			phrase: `Twisted and maniacal - a slathering testament to the powers of corruption!`
		},
		{
			group: fight,
			type: bad,
			situation: bossFight,
			audio: `aMarvelOfTechology`,
			phrase: `A marvel of technology - an engine of destruction!`
		},
		{
			group: fight,
			type: horror,
			situation: bossFight,
			audio: `itIsATravesty`,
			phrase: `It is a travesty - a blundering mountain of hatred and rage.`
		},
		{
			group: fight,
			type: horror,
			situation: bossFight,
			audio: `squirmingContortingAndEverExpanding`,
			phrase: `Squirming, contorting and ever-expanding...this horror must be unmade!`
		},
		{
			group: fight,
			type: bad,
			situation: bossFight,
			audio: `theQueenAndTheirSlave`,
			phrase: `The aquatic devils have remade the poor girl in their image! She is their queen, and their slave!`
		},
		{
			group: fight,
			type: bad,
			situation: bossFight,
			audio: `theCaptainShoutsHisOrdersAndTheCrewObeys`,
			phrase: `Even in death, the captain shouts his orders, and the crew obeys...`
		},
		/*        {
		            group: fight,
		            type: horror,
		            situation: bossFight,
					audio: ``,
					phrase: `A lifetime of pious toil, an eternity of suffering.`
		        },
		        {
		            group: fight,
		            type: misc,
		            situation: bossFight,
					audio: ``,
					phrase: `Ha! The poor fool still stands, battered and broken as his precious mill.`
		        },
		        {
		            group: fight,
		            type: misc,
		            situation: bossFight,
					audio: ``,
					phrase: `Fitting, that he find his rest upon the dirt he harrowed to fruitlessly.`
		        },
		        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `Witness the woundrous fury of the Stars!`
		        },
		        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `A star-spawned horror rattles its crystalline cage!`
		        },
		        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `Shattered and unmade! Or, perhaps, reborn?`
		        },
		        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `It will live again in another time, another place.`
		        },
		        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `Who could fathom the hateful scorn of the swirling stars!`
		        },
		*/
		{
			group: fight,
			type: horror,
			situation: bossFight,
			audio: `piledhigh`,
			phrase: `The twisted faces of the damned, piled high and cloaked in malice!`
		},
		{
			group: fight,
			type: bad,
			situation: bossFight,
			audio: `theSparklingEyesOfYouth`,
			phrase: `The sparkling eyes of youth - twisted and made merciless!`
		},
		{
			group: fight,
			type: misc,
			situation: bossFight,
			audio: `ghoulishCollection`,
			phrase: `As the ghoulish collection scatters, the rats prepare to feast.`
		},
		{
			group: fight,
			type: uplift,
			situation: bossFight,
			audio: `aPredator`,
			phrase: `A predator is often blind to its own peril.`
		},
		{
			group: fight,
			type: horror,
			situation: bossFight,
			audio: `infiniteMalignityOfTheStars`,
			phrase: `Behold the infinite malignity of the stars!`
		},
		{
			group: fight,
			type: horror,
			situation: bossFight,
			audio: `aStarspawnedHorror`,
			phrase: `A star-spawned horror!`
		},
		{
			group: fight,
			type: bad,
			situation: bossFight,
			audio: `theSpaceBetweenWorlds`,
			phrase: `The space between worlds is no place for mortal men.`
		},
		{
			group: fight,
			type: horror,
			situation: bossFight,
			audio: `aFeverDream`,
			phrase: `It could be dismissed as a fever dream, if not for the corpses.`
		},
		/*        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `A denizen of unconscionable alienage.`
		        },
		        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `A shard of alien malignity!`
		        },
		        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `A lurching composition of otherworldly death!`
		        },
		        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `A shuddering crystalline bulk!`
		        },
		        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `Born of the void, it dies in the Earth!`
		        },
		        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `Banished in the void!`
		        },
		        {
		            group: ``,
		            type: ``,
					audio: ``,
					phrase: `It came from the stars, let it return to them.`
		        },
		*/
		{
			group: torch,
			type: good,
			situation: torch100,
			audio: `theMatchIsStruck`,
			phrase: `The match is struck. A blazing star is born!`
		},
		{
			group: torch,
			type: middle,
			situation: torch100,
			audio: `thePathIsClear`,
			phrase: `The way is lit. The path is clear. We require only the strength to follow it.`
		},
		{
			group: torch,
			type: good,
			situation: torch100,
			audio: `inRadianceMayWeFindVictory`,
			phrase: `In radiance may we find victory.`
		},
		{
			group: torch,
			type: good,
			situation: torch100,
			audio: `asTheLightGains`,
			phrase: `As the light gains purchase, spirits are lifted and purpose is made clear.`
		},
		{
			group: torch,
			type: good,
			situation: torch100,
			audio: `thePromiseOfSafety`,
			phrase: `The light, the promise of safety!`
		},
		{
			group: torch,
			type: bad,
			situation: torch0,
			audio: `trickeryAndBoogiemen`,
			phrase: `The darkness holds much worse than mere trickery and bogeymen.`
		},
		{
			group: torch,
			type: bad,
			situation: torch0,
			audio: `hauntingTheHeathsOfMen`,
			phrase: `Darkness closes in, haunting the hearts of men.`
		},
		{
			group: torch,
			type: middle,
			situation: torch0,
			audio: `aGlintOfGold`,
			phrase: `Terrors may indeed stalk these shadows, but yonder – a glint of gold."`
		},
		{
			group: torch,
			type: middle,
			situation: torch0,
			audio: `secretsAndWonders`,
			phrase: `Secrets and wonders can be found in the most tenebrous corners of this place.`
		},
		{
			group: torch,
			type: bad,
			situation: torch0,
			audio: `theDarknessHoldsDominion`,
			phrase: `And now... the darkness holds dominion – black as death.`
		},
		{
			group: dungeon,
			type: bad,
			situation: camping,
			audio: `circleInTheDark`,
			phrase: `Circled in the dark, the battle may yet be won.`
		},
		{
			group: dungeon,
			type: bad,
			situation: camping,
			audio: `sparkWithotKingling`,
			phrase: `A spark without kindling is a goal without hope.`
		},
		{
			group: dungeon,
			type: bad,
			situation: camping,
			audio: `gatheredClose`,
			phrase: `Gathered close in tenuous firelight, and uneasy companionship.`
		},
		{
			group: dungeon,
			type: good,
			situation: camping,
			audio: `momentOfRespite`,
			phrase: `A moment of respite. A chance to steel oneself against the coming horrors.`
		},
		{
			group: dungeon,
			type: bad,
			situation: camping,
			audio: `ratsInAMaze`,
			phrase: `Huddled together, furtive and vulnerable. Rats in a maze.`
		},
		{
			group: dungeon,
			type: misc,
			situation: trapHit,
			audio: `singularPurpouse`,
			phrase: `Cruel machinations spring to life with a singular purpose!`
		},
		{
			group: dungeon,
			type: misc,
			situation: trapHit,
			audio: `curiousIsTheTrapmakersArt`,
			phrase: `Curious is the trap-maker's art... his efficacy unwitnessed by his own eyes.`
		},
		{
			group: dungeon,
			type: misc,
			situation: trapHit,
			audio: `mechanicalHazards`,
			phrase: `Mechanical hazards, possessed by evil intent.`
		},
		{
			group: dungeon,
			type: bad,
			situation: trapHit,
			audio: `ambushedByFoulInvention`,
			phrase: `Ambushed by foul invention!`
		},
		{
			group: dungeon,
			type: bad,
			situation: trapHit,
			audio: `ancientTraps`,
			phrase: `Ancient traps lie in wait, unsprung and thirsting for blood.`
		},
		{
			group: dungeon,
			type: bad,
			situation: trapHit,
			audio: `carelessnesswillFindNoClemency`,
			phrase: `Carelessness will find no clemency in this place!`
		},
		{
			group: dungeon,
			type: middle,
			situation: trapHit,
			audio: `watchYourStep`,
			phrase: `Watch your step.`
		},
		{
			group: dungeon,
			type: smug,
			situation: trapHit,
			audio: `suchMistakesAreTheExceptionAndNotTheRule`,
			phrase: `Mind that such missteps are the exception, and not the rule.`
		},
		{
			group: dungeon,
			type: misc,
			situation: obstacle,
			audio: `bentOnPreventingPassage`,
			phrase: `Even the cold stone seems bent on preventing passage.`
		},
		{
			group: dungeon,
			type: misc,
			situation: obstacle,
			audio: `predateEvenTheEarliestSettlers`,
			phrase: `Such blockages are unsurprising – these tunnels predate even the earliest settlers.`
		},
		{
			group: dungeon,
			type: misc,
			situation: obstacle,
			audio: `natureHerself`,
			phrase: `Nature herself - a victim to this spreading corruption: malformed with misintent.`
		},
		{
			group: dungeon,
			type: misc,
			situation: obstacle,
			audio: `anotherMariner`,
			phrase: `Another mariner... Another misfortune.`
		},
		{
			group: dungeon,
			type: bad,
			situation: obstacle,
			audio: `relyOnFlesh`,
			phrase: `Without tools of iron, you must rely on flesh and indefatigable purpose.`
		},
		{
			group: dungeon,
			type: bad,
			situation: hunger,
			audio: `turningTheBodyAgainstHimself`,
			phrase: `Gnawing hunger sets in, turning the body against itself, weakening the mind…`
		},
		{
			group: dungeon,
			type: bad,
			situation: hunger,
			audio: `aBiteOfBread`,
			phrase: `To fall for such a little thing... a bite of bread...`
		},
		{
			group: dungeon,
			type: misc,
			situation: hunger,
			audio: `packsFullOfSteel`,
			phrase: `Packs full of steel and war, but nary of thought given to the plow.`
		},
		{
			group: dungeon,
			type: bad,
			situation: hunger,
			audio: `overComeAFailingBody`,
			phrase: `No force of will can overcome a failing body.`
		},
		{
			group: dungeon,
			type: bad,
			situation: hunger,
			audio: `emptyStomach`,
			phrase: `The requirements of survival cannot be met on an empty stomach.`
		},
		{
			group: fight,
			type: middle,
			situation: heal,
			audio: `soothedSedated`,
			phrase: `Soothed, sedated.`
		},
		{
			group: fight,
			type: good,
			situation: heal,
			audio: `momentaryAbatement`,
			phrase: `A momentary abatement...`
		},
		{
			group: fight,
			type: middle,
			situation: heal,
			audio: `theWoundsOfWar`,
			phrase: `The wounds of war can be healed, but never hidden.`
		},
		{
			group: fight,
			type: middle,
			situation: heal,
			audio: `theFeveredPitchOfBattle`,
			phrase: `Compassion is a rarity in the fevered pitch of battle.`
		},
		{
			group: fight,
			type: good,
			situation: heal,
			audio: `surgicalPressition`,
			phrase: `Surgical precision!`
		},
		{
			group: fight,
			type: good,
			situation: heal,
			audio: `vigorIsRestored`,
			phrase: `Vigor is restored!`
		},
		{
			group: fight,
			type: uplift,
			situation: heal,
			audio: `theBloodPumps`,
			phrase: `The blood pumps, the limbs obey!`
		},
		{
			group: fight,
			type: middle,
			situation: heal,
			audio: `theFleshIsNit`,
			phrase: `The flesh is knit!`
		},
		{
			group: fight,
			type: downpushing,
			situation: heal,
			audio: `patchedUp`,
			phrase: `Patched up, if only to bleed again.`
		},
		{
			group: fight,
			type: downpushing,
			situation: heal,
			audio: `itCanBePostponed`,
			phrase: `Death cannot be escaped! But it can be postponed.`
		},
		{
			group: fight,
			type: middle,
			situation: heal,
			audio: `deathDenied`,
			phrase: `A death denied for now.`
		},
		{
			group: fight,
			type: downpushing,
			situation: heal,
			audio: `deathIsPatient`,
			phrase: `Death is patient, it will wait.`
		},
		{
			group: fight,
			type: uplift,
			situation: kill,
			audio: `faintHopeBlossoms`,
			phrase: `As the fiend falls, a faint hope blossoms.`
		},
		{
			group: fight,
			type: good,
			situation: kill,
			audio: `theEnemyCrumbles`,
			phrase: `Confidence surges as the enemy crumbles!`
		},
		{
			group: fight,
			type: good,
			situation: kill,
			audio: `pressthisAdvantage`,
			phrase: `Press this advantage, give them no quarter!`
		},
		{
			group: fight,
			type: good,
			situation: kill,
			audio: `maintainTheOffensive`,
			phrase: `Their formation is broken - maintain the offensive.`
		},
		{
			group: fight,
			type: epic,
			situation: kill,
			audio: `destroyThemAll`,
			phrase: `Continue the onslaught! Destroy. Them. All.`
		},
		{
			group: fight,
			type: epic,
			situation: kill,
			audio: `executedWithImpunity`,
			phrase: `Executed with impunity!`
		},
		{
			group: fight,
			type: good,
			situation: weakKill,
			audio: `cleansedFromOurLands`,
			phrase: `Another abomination cleansed from our lands.`
		},
		{
			group: fight,
			type: good,
			situation: weakKill,
			audio: `begoneFiend`,
			phrase: `Begone, fiend!`
		},
		{
			group: fight,
			type: good,
			situation: weakKill,
			audio: `backToThePit`,
			phrase: `Back to the pit!`
		},
		{
			group: fight,
			type: good,
			situation: weakKill,
			audio: `anotherOneFalls`,
			phrase: `Another one falls!`
		},
		{
			group: fight,
			type: good,
			situation: strongKill,
			audio: `decimated`,
			phrase: `Decimated!`
		},
		{
			group: fight,
			type: good,
			situation: strongKill,
			audio: `obliterated`,
			phrase: `Obliterated!`
		},
		{
			group: fight,
			type: epic,
			situation: strongKill,
			audio: `destroyed`,
			phrase: `Destroyed!`
		},
		{
			group: fight,
			type: epic,
			situation: strongKill,
			audio: `erradicated2`,
			phrase: `Eradicated!`
		},
		{
			group: fight,
			type: epic,
			situation: strongKill,
			audio: `anihilated`,
			phrase: `Annihilated!`
		},
		{
			group: fight,
			type: smug,
			situation: bigKill,
			audio: `disuadeTheSharpestBlade`,
			phrase: `Prodigious size alone does not dissuade the sharpened blade.`
		},
		{
			group: fight,
			type: good,
			situation: bigKill,
			audio: `theirCursedChampionFalls`,
			phrase: `Their cursed champion falls!`
		},
		{
			group: fight,
			type: smug,
			situation: bigKill,
			audio: `noIntrinsicMerit`,
			phrase: `Monstrous size has no intrinsic merit, unless inordinate exsanguination be considered a virtue.`
		},
		{
			group: fight,
			type: good,
			situation: bigKill,
			audio: `theBiggerTheBeast`,
			phrase: `The bigger the beast, the greater the glory.`
		},
		{
			group: fight,
			type: good,
			situation: bigKill,
			audio: `perhapsATurningPoint`,
			phrase: `A victory - perhaps a turning point.`
		},
		{
			group: fight,
			type: middle,
			situation: blightOrBleed,
			audio: `deathByInches`,
			phrase: `A death by inches...`
		},
		{
			group: fight,
			type: good,
			situation: blightOrBleed,
			audio: `cutsOnItsOwn`,
			phrase: `Great is the weapon that cuts on its own!`
		},
		{
			group: fight,
			type: middle,
			situation: blightOrBleed,
			audio: `slowlyGently`,
			phrase: `Slowly, gently, this is how a life is taken...`
		},
		{
			group: fight,
			type: middle,
			situation: blightOrBleed,
			audio: `unforseenUnforgiving`,
			phrase: `The slow death - unforeseen, unforgiving.`
		},
		{
			group: fight,
			type: good,
			situation: hit,
			audio: `decisivePummeling`,
			phrase: `A decisive pummelling!`
		},
		{
			group: fight,
			type: good,
			situation: hit,
			audio: `aPowerfullBlow`,
			phrase: `A powerful blow!`
		},
		{
			group: fight,
			type: epic,
			situation: hit,
			audio: `aDevastatingBlow2`,
			phrase: `A devastating blow!`
		},
		{
			group: fight,
			type: good,
			situation: hit,
			audio: `impressive`,
			phrase: `Impressive!`
		},
		{
			group: fight,
			type: epic,
			situation: hit,
			audio: `theGroundQuakes`,
			phrase: `The ground quakes!`
		},
		{
			group: fight,
			type: epic,
			situation: hit,
			audio: `aSingularStrike2`,
			phrase: `A singular strike!`
		},
		{
			group: fight,
			type: good,
			situation: hit,
			audio: `wellStruck2`,
			phrase: `Well struck!`
		},
		{
			group: fight,
			type: good,
			situation: hit,
			audio: `presitionAndPower`,
			phrase: `Precision and power!`
		},
		{
			group: fight,
			type: good,
			situation: hit,
			audio: `masterfullyExecuted`,
			phrase: `Masterfully executed!`
		},
		{
			group: fight,
			type: horrible,
			situation: beingHit,
			audio: `howquicklyTheTidesTurn`,
			phrase: `How quickly the tide turns!`
		},
		{
			group: fight,
			type: horrible,
			situation: beingHit,
			audio: `mortalityClarifiedInASingleStrike`,
			phrase: `Mortality clarified in a single strike!`
		},
		{
			group: fight,
			type: bad,
			situation: beingHit,
			audio: `grieviousInjury`,
			phrase: `Grievous injury, palpable fear...`
		},
		{
			group: fight,
			type: bad,
			situation: beingHit,
			audio: `cannotBeLestUnanswered`,
			phrase: `Such a terrible assault cannot be left unanswered!`
		},
		{
			group: fight,
			type: bad,
			situation: beingHit,
			audio: `deathWaits`,
			phrase: `Death waits for the slightest lapse in concentration.`
		},
		{
			group: fight,
			type: horrible,
			situation: beingHit,
			audio: `exposedToAKillingBlow`,
			phrase: `Exposed to a killing blow!`
		},
		{
			group: fight,
			type: bad,
			situation: beingHit,
			audio: `theEndApproaches`,
			phrase: `Ringing ears, blurred vision - the end approaches...`
		},
		{
			group: fight,
			type: bad,
			situation: beingHit,
			audio: ``,
			phrase: `Dazed, reeling, about to break...`
		},
		{
			group: fight,
			type: bad,
			situation: beingHit,
			audio: `unnervedUnbalanced`,
			phrase: `Unnerved, unbalanced...`
		},
		{
			group: fight,
			type: bad,
			situation: beingHit,
			audio: `aDizzingBlow`,
			phrase: `A dizzying blow to body and brain!`
		},
		{
			group: fight,
			type: bad,
			situation: beingDebuffed,
			audio: `weakend`,
			phrase: `Weakened!`
		},
		{
			group: fight,
			type: bad,
			situation: beingDebuffed,
			audio: `diminished`,
			phrase: `Diminished!`
		},
		{
			group: fight,
			type: bad,
			situation: beingDebuffed,
			audio: `theWillToFight`,
			phrase: `The will to fight falters!`
		},
		{
			group: fight,
			type: bad,
			situation: beingDebuffed,
			audio: `confusion`,
			phrase: `Confusion, nerves, and panic!`
		},
		{
			group: fight,
			type: bad,
			situation: beingHorrorized,
			audio: `gnawingIncertanty`,
			phrase: `Gnawing uncertainty - the birthplace of dread.`
		},
		{
			group: fight,
			type: horrible,
			situation: beingHorrorized,
			audio: `festeringFear`,
			phrase: `Festering fear consumes the mind!`
		},
		{
			group: fight,
			type: horrible,
			situation: beingHorrorized,
			audio: `theHorror`,
			phrase: `The horror...`
		},
		{
			group: fight,
			type: bad,
			situation: beingHorrorized,
			audio: `theAbyssReturnsEvenTheBoldestGaze`,
			phrase: `The abyss returns even the boldest gaze.`
		},
		{
			group: fight,
			type: middle,
			situation: beingBuffed,
			audio: `theBloodQuickens`,
			phrase: `The blood quickens!`
		},
		{
			group: fight,
			type: epic,
			situation: beingBuffed,
			audio: `aBrilliantConfluenceOfSkillAndPurpouse`,
			phrase: `A brilliant confluence of skill and purpose!`
		},
		{
			group: fight,
			type: epic,
			situation: beingBuffed,
			audio: `aTimeToPerformBeyoindOnesLimits`,
			phrase: `A time to perform beyond one's limits!`
		},
		{
			group: fight,
			type: good,
			situation: beingBuffed,
			audio: `inspirationAndImprovement`,
			phrase: `Inspiration and improvement!`
		},
		{
			group: fight,
			type: bad,
			situation: deathDoor,
			audio: `oblivion`,
			phrase: `Perched at the very precipice of oblivion...`
		},
		{
			group: fight,
			type: bad,
			situation: deathDoor,
			audio: `becomingUnwound`,
			phrase: `A hand-breadth from becoming unwound...`
		},
		{
			group: fight,
			type: bad,
			situation: deathDoor,
			audio: `facingTheAbyss`,
			phrase: `Teetering on the brink, facing the abyss...`
		},
		{
			group: fight,
			type: horrible,
			situation: deathDoor,
			audio: `theTrueTest`,
			phrase: `And now the true test... hold fast, or expire?`
		},
		{
			group: fight,
			type: bad,
			situation: deathDoor,
			audio: `terribleVistasOfEmptyness`,
			phrase: `As life ebbs, terrible vistas of emptiness reveal themselves.`
		},
		{
			group: fight,
			type: bad,
			situation: deathBlow,
			audio: `survivalIsATrnuosProposition`,
			phrase: `Survival is a tenuous proposition in this sprawling tomb.`
		},
		{
			group: fight,
			type: bad,
			situation: deathBlow,
			audio: `moreBloodSoaksTheSoil`,
			phrase: `More blood soaks the soil, feeding the evil therein.`
		},
		{
			group: fight,
			type: downpushing,
			situation: deathBlow,
			audio: `anotherLifeWasted`,
			phrase: `Another life wasted in the pursuit of glory and gold.`
		},
		{
			group: fight,
			type: downpushing,
			situation: deathBlow,
			audio: `noPlaceForTheWeak`,
			phrase: `This is no place for the weak, or the foolhardy.`
		},
		{
			group: fight,
			type: downpushing,
			situation: deathBlow,
			audio: `noPlaceForTheWeak2`,
			phrase: `This is no place for the weak, or foolhardy.`
		},
		{
			group: fight,
			type: downpushing,
			situation: deathBlow,
			audio: `moreDustMoreAshesMoreDisapointment`,
			phrase: `More dust, more ashes, more disappointment.`
		},
		{
			group: fight,
			type: uplift,
			situation: win,
			audio: `theyCanBeBeaten`,
			phrase: `These nightmarish creatures can be felled! They can be beaten!`
		},
		{
			group: fight,
			type: uplift,
			situation: win,
			audio: `seizeThisMomentum`,
			phrase: `Seize this momentum! Push on to the task's end!`
		},
		{
			group: fight,
			type: good,
			situation: win,
			audio: `thisExpeditionAtLeastPromisesSuccess`,
			phrase: `This expedition, at least, promises success.`
		},
		{
			group: fight,
			type: downpushing,
			situation: win,
			audio: `asVictoriesMount`,
			phrase: `As victories mount, so too will resistance.`
		},
		{
			group: fight,
			type: TOPTIER,
			situation: win,
			audio: `successSoClearlyInView`,
			phrase: `Success so clearly in view... or is it merely a trick of the light?`
		},
		{
			group: fight,
			type: TOPTIER,
			situation: win,
			audio: `REMINDYOURSELFTHATOVERCONFIDENCEISASLOWANDINCIDIOUSKILLER`,
			phrase: `Remind yourself that overconfidence is a slow and insidious killer.`
		},
		{
			group: fight,
			type: uplift,
			situation: win,
			audio: `aTrifflingVictory`,
			phrase: `A trifling victory, but a victory nonetheless.`
		},
		{
			group: fight,
			type: downpushing,
			situation: win,
			audio: `precipitatesADizzianFall`,
			phrase: `Be wary - triumphant pride precipitates a dizzying fall...`
		},
		{
			group: fight,
			type: middle,
			situation: win,
			audio: `foolishHorrors`,
			phrase: `Ghoulish horrors - brought low and driven into the mud!`
		},
		{
			group: fight,
			type: smug,
			situation: fullInventory,
			audio: `ifYouValueSuchThings`,
			phrase: `Impressive haul! If you value such things.`
		},
		{
			group: fight,
			type: smug,
			situation: fullInventory,
			audio: `neatlyOrdered`,
			phrase: `Ornaments neatly ordered, lovingly admired.`
		},
		{
			group: fight,
			type: downpushing,
			situation: fullInventory,
			audio: `ristsLifeAndLimb`,
			phrase: `Such a burden of finery risks life and limb.`
		},
		{
			group: fight,
			type: downpushing,
			situation: fullInventory,
			audio: `oftenAttractsUnwantedAttention	`,
			phrase: `A full pack often attracts unwanted attention.`
		},
		{
			group: fight,
			type: bad,
			situation: escapeFail,
			audio: `trueDesperation`,
			phrase: `True desperation is known only when escape is impossible.`
		},
		{
			group: fight,
			type: bad,
			situation: escapeFail,
			audio: `corneredTrapped`,
			phrase: `Cornered! Trapped! And forced to fight on...`
		},
		{
			group: fight,
			type: bad,
			situation: escapeFail,
			audio: `willThisBeAMassacre`,
			phrase: `No chance for egress - will this be a massacre?`
		},
		{
			group: fight,
			type: uplift,
			situation: escape,
			audio: `thisSkirmishMayBeLost`,
			phrase: `This skirmish may be lost, but the battle may yet be won.`
		},
		{
			group: fight,
			type: uplift,
			situation: escape,
			audio: `regroups`,
			phrase: `A wise general cuts losses, and regroups.`
		},
		{
			group: fight,
			type: uplift,
			situation: escape,
			audio: `failingToRecognizeIt`,
			phrase: `The sin is not in being outmatched, but in failing to recognize it.`
		},
		{
			group: fight,
			type: middle,
			situation: stressAt50,
			audio: `heroismOrCowardice`,
			phrase: `Injury and despondence set the stage for heroism... or cowardice.`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `fragileLikeARobinsEgg`,
			phrase: `The human mind - fragile like a robin's egg.`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `heroism`,
			phrase: `Wherefore, heroism?`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `whistandSuchAnAssault`,
			phrase: `The mind cannot hope to withstand such an assault.`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `falltoTheTempestWinds`,
			phrase: `Even the aged oak will fall to the tempest's winds.`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `ourOldFriend`,
			phrase: `Madness, our old friend!`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `clarityInMadness`,
			phrase: `One can sometimes find clarity in madness, but only rarely...`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `sublimityOfTheIntelligence`,
			phrase: `Madness - sublimity of the intelligence, or so it has been said.`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `theBulwarkOfTheMind`,
			phrase: `The bulwarks of the mind have fallen!`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `beholdTheAbyss`,
			phrase: `The abyss is made manifest!`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `frustrationAndFury`,
			phrase: `Frustration and fury, more destructive than a hundred cannons.`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `fearAndFrailty`,
			phrase: `Fear and frailty finally claim their due.`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `theShadowsWhisperOfConspiracy`,
			phrase: `The walls close in, the shadows whisper of conspiracy!`
		},
		{
			group: fight,
			type: horrible,
			situation: afflicted,
			audio: `noHopeInThisHell`,
			phrase: `There can be no hope in this hell, no hope at all.`
		},
		{
			group: fight,
			type: horrible,
			situation: afflicted,
			audio: `selfpreservationIsParamount`,
			phrase: `Self-preservation is paramount - at any cost!`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `thoseWhoCarveInjury`,
			phrase: `Those who covet injury find it in no short supply.`
		},
		{
			group: fight,
			type: bad,
			situation: afflicted,
			audio: `reelingGasping`,
			phrase: `Reeling, gasping, taken over the edge into madness!`
		},
		{
			group: fight,
			type: good,
			situation: virtuous,
			audio: `aMomentOfValor`,
			phrase: `A moment of valor shines brightest against a backdrop of despair.`
		},
		{
			group: fight,
			type: good,
			situation: virtuous,
			audio: `adversityCanFosterHope`,
			phrase: `Adversity can foster hope, and resilience.`
		},
		{
			group: fight,
			type: good,
			situation: virtuous,
			audio: `aMomentOfClarity`,
			phrase: `A moment of clarity in the eye of the storm...`
		},
		{
			group: fight,
			type: good,
			situation: virtuous,
			audio: `angerIsPower`,
			phrase: `Anger is power - unleash it!`
		},
		{
			group: fight,
			type: good,
			situation: virtuous,
			audio: ``,
			phrase: `Many fall in the face of chaos; but not this one, not today.`
		},
	],

	largePhrases: [{
			group: ``,
			type: ``,
			audio: ``,
			phrase: `There is a place, beneath those ancient ruins, in the moor, that calls out to the boldest among them... "We are the Flame!" they cry, "And Darkness fears us!" They descend, spurred on by fantasies of riches and redemption, to lay bare whatever blasphemous abnormality may slumber restlessly in that unholy abyss... But Darkness is insidious. Terror and Madness can find cracks in the sturdiest of armors, the most resolute of minds... And below, in that limitless chasm of Chaos, they will realize the truth of it. "We are not the Flame!" they will cry out, "We are but moths and we are DOOMED!" And their screams will echo amidst the pitiless cyclopean stones... Of the Darkest Dungeon.`
		},
		{
			group: ``,
			type: ``,
			audio: ``,
			phrase: `Ruin has come to our family. You remember our venerable house, opulent and imperial. Gazing proudly from its stoic perch above the moor. I lived all my years in that ancient, rumor shadowed manor. Fattened by decadence and luxury. And yet, I began to tire of conventional extravagance. Singular, unsettling tales suggested the mansion itself was a gateway to some fabulous and unnamable power. With relic and ritual, I bent every effort towards the excavation and recovery of those long buried secrets, exhausting what remained of our family fortune on swarthy workmen and sturdy shovels. At last, in the salt-soaked crags beneath the lowest foundations we unearthed that damnable portal and antediluvian evil. Our every step unsettled the ancient earth but we were in a realm of death and madness! In the end, I alone fled laughing and wailing through those blackened arcades of antiquity. Until consciousness failed me. You remember our venerable house, opulent and imperial. It is a festering abomination! I beg you, return home, claim your birthright, and deliver our family from the ravenous clutching shadows of the Darkest Dungeon.`
		},
		{
			group: ``,
			type: ``,
			audio: ``,
			phrase: `You will arrive along the old road. It winds with a troubling, serpent-like suggestion through the corrupted countryside. Leading only, I fear, to ever more tenebrous places. There is a sickness in the ancient pitted cobbles of the old road and on its writhing path you will face viciousness, violence, and perhaps other damnably transcendent terrors. So steel yourself and remember: there can be no bravery without madness. The old road will take you to hell, but in that gaping abyss we will find our redemption.`
		},
		{
			group: ``,
			type: ``,
			audio: ``,
			phrase: `In those younger years my home was a hive of unbridled hedonism, a roiling apiary where instinct and impulse were indulged with wild abandon. A bewitching predator slipped in amidst the swarm of tittering sycophants. Though outwardly urbane, I could sense in her a mocking thirst. Driven half-mad by cloying vulgarity I plotted to rid myself of this lurking threat in a grand display of sadistic sport. But as the moment of murder drew nigh, the gibbous moon revealed her inhuman desires in all their stultifying hideousness…`
		},
		{
			group: ``,
			type: ``,
			audio: ``,
			phrase: `Mercifully, the morbid encounter resolved itself in my favor, and I set to work pursuing degeneracy in its most decadent forms. The air pulsed with anticipation as I revealed the unnatural terroir of the house vintage. But my exultation was cut short as the attending gentry turned upon themselves in an orgy of an indescribable frenzy. A single drop of that forbidden tannin gifted me with a dizzying glimpse of a hibernating horror beneath my feet, and in that moment, I understood the terrible truth of the world. I stood reborn, molted by newfound knowledge, my head throbbing to the growing whine of winged vermin come to drink the tainted blood… of The Darkest Dungeon.`
		},
		{
			group: ``,
			type: ``,
			audio: `excavationBeneathTheMannor`,
			phrase: `Excavations beneath the manor were well underway, when a particular ragged indigent arrived in the hamlet. This filthy, toothless miscreant boasted an uncanny knowledge of my ambitions and prognosticated publicly that left unchecked, I would soon unleash doom upon the world.`
		},
		{
			group: ``,
			type: ``,
			audio: `thisReavingCreatureHadToBeSilenced`,
			phrase: `This raving creature had to be silenced, but, to my bafflement, doing so proved maddeningly impossible. How had he survived the stockades, the icy waters, and the knives I delivered so enthusiastically into his back? How had he returned time and time again to rouse the townsfolk with his wild speculations and prophecies?`
		},
		{
			group: ``,
			type: ``,
			audio: `iShowedHimTheThing`,
			phrase: `Finally, resigned to his uncommon corporeal resilience, I lured him to the dig. There, I showed him the Thing, and detailed the full extent of my plans. Triumphantly, I watched as he tore his eyes from their sockets, and ran shrieking into the shadows - wailing maniacally that the end was upon us all.`
		},
		{
			group: ``,
			type: ``,
			audio: `masteryoverLifeAndDeath`,
			phrase: `Mastery over life and death was chief among my early pursuits. I began in humility, but my ambition was limitless. Who could have divined the prophetic import of something as unremarkable as a twitch in the leg of a dead rat?`
		},
		{
			group: ``,
			type: ``,
			audio: `iMurderedThemAsTheySlept`,
			phrase: `I entertained a delegation of experts from overseas, eager to plumb the depths of their knowledge and share with them certain techniques and alchemical processes I had found to yield wondrous and terrifying results. Having learned all I could from my visiting guests, I murdered them as they slept.`
		},
		{
			group: ``,
			type: ``,
			audio: `iBroughtMyColleaguesBackFromTheDead`,
			phrase: `I brought my colleagues back with much of their intellect intact, a remarkable triumph for even the most experienced necromancer. Freed from the trappings of their humanity, they plied their terrible trade anew - the dead reviving the dead, on and on down the years... forever.`
		},
		{
			group: ``,
			type: ``,
			audio: `myWorkWasInterruptedHowever	`,
			phrase: `I had collected many rare and elusive volumes on ancient herbal properties, and was set to enjoy several weeks immersed in comfortable study. My work was interrupted, however, by a singularly striking young woman who insisted on repeated calls to the house.`
		},
		{
			group: ``,
			type: ``,
			audio: `weBeganToPlantHarvestAndBrew`,
			phrase: `Her knowledge of horticulturalism, and its role in various arcane practices impressed me greatly. My licentious impulse gave way to a genuine, professional respect, and together, we began to plant, harvest, and brew.`
		},
		{
			group: ``,
			type: ``,
			audio: `toLeaveInTheWeald`,
			phrase: `As time wore on, her wild policy of self-experimentation grew intolerable. She quaffed all manner of strange fungii, herbs and concoctions, intent on gaining some insight into the horror we both knew to be growing beneath us. The change in her was appalling, and, no longer able to stomach it, I sent her to live in the Weald, where her wildness would be welcomed.`
		},
		{
			group: ``,
			type: ``,
			audio: `rumoursOfMyMorbidGenious`,
			phrase: `Simple folk are by their nature loquacious, and the denizens of the Hamlet were no exception. It was not long before rumors of my morbid genius and secretive excavations began to fuel local legend. In the face of my increasingly egregious flaunting of public taboos, awe turned to ire, and demonstrations were held in the town square.`
		},
		{
			group: ``,
			type: ``,
			audio: `theWildWhispersOfHeresy`,
			phrase: `The wild whispers of heresy roused the rabble to violent action. Such was the general air of rebellion that even my generous offer of gold to the local constabulary was rebuffed! To reassert my rule, I sought out unscrupulous men skilled in the application of force. Tight-lipped and terrifying, these mercenaries brought with them a war machine of terrible implication.`
		},
		{
			group: ``,
			type: ``,
			audio: `thePopulationOfTheHamletWasCutOff`,
			phrase: `Eager to end the tiresome domestic distraction, I instructed my newly formed militia of hardened bandits, brigands and killers to go forth and do their work. Compliance and order were restored, and the noisome population of the Hamlet was culled to more... manageable numbers.`
		},
		{
			group: ``,
			type: ``,
			audio: `theProcessCanFailSpectacurally`,
			phrase: `The ways and rituals of blood sacrifice are difficult to master. Those from beyond require a physical vessel if they are to make the crossing into our reality. The timing of the chants is imperative - without the proper utterances at precise intervals, the process can fail spectacularly.`
		},
		{
			group: ``,
			type: ``,
			audio: `crudeAndDesapointing`,
			phrase: `My first attempts at summoning were crude and the results, disappointing. I soon found however, that the type and condition of the host's meat was a critical factor. The best results came from pigs, whose flesh is most like that of man.`
		},
		{
			group: ``,
			type: ``,
			audio: `brutishAndStupid`,
			phrase: `The Great Thing I had managed to bring through was brutish and stupid. Moreover, it required prodigious amounts of meat to sustain itself, but this was only a trifling concern – after all, I had a village full of it.`
		},
		{
			group: ``,
			type: ``,
			audio: `broughtOnlyFailureAndDisapointing`,
			phrase: `My zeal for blood rituals and summoning rites had begun to ebb as each attempt invariably brought only failure, and disappointment. Progress was halting, and the rapidly accumulating surplus of wasted flesh had become... burdensome.`
		},
		{
			group: ``,
			type: ``,
			audio: `ancientNetworkOfAqueducts`,
			phrase: `I could not store such a prodigious amount of offal, nor could I rid myself of it easily, possessed as was by unnameable things from outer spheres. When excavations beneath the Manor broke through into an ancient network of aqueducts and tunnels, I knew I had found a solution to the problem of disposal.`
		},
		{
			group: ``,
			type: ``,
			audio: `atLastIWasRideOfThem`,
			phrase: `The spasmodically squirming, braying, and snorting half-corpses were heaped each upon the other until at last I was rid of them. The Warrens became a landfill of snout and hoof, gristle and bone – a mountainous, twitching mass of misshapen flesh fusing itself together in the darkness.`
		},
		{
			group: ``,
			type: ``,
			audio: `concernForMolestation`,
			phrase: `My lofty position wasn't always accompanied by the fear of office, and there was a time I could walk the streets or raise a glass in the tavern without concern for molestation. Faithful as the tide, one precocious village waif made it her hobby to shadow my every errand. It was charming then, troublesome later.`
		},
		{
			group: ``,
			type: ``,
			audio: `thePactStruck`,
			phrase: `In financial desperation, I struck a bargain with the ancient things that surfaced in search of sacrifice when the moon was right. Their price was the delivery of an obscure idol and one other item of more troubling portent. The pact struck, my newfound accomplices slipped silently beneath the brackish water. A fearful stirring at the edge of the torchlight betrayed a familiar witness, and gifted me with malign inspiration.`
		},
		{
			group: ``,
			type: ``,
			audio: `underTheBloodMoon`,
			phrase: `Under the blood moon, I lured my wide-eyed prey to the pier’s edge. Before she could properly appreciate her position, I clamped on a manacle, chaining her to the leering idol. A small push was sufficient to send both into the icy waters. And when at length the tide receded, jewels of the most magnificent grandeur lay scattered upon the shore.`
		},
		{
			group: ``,
			type: ``,
			audio: `marineShipments`,
			phrase: `Prying eyes had become a nuisance along the Old Road, and so I undertook to receive my most curious deliveries by way of marine shipments. A sheltered jetty was accessible by a narrow stone stair off the back of the manor, and a discreet system of pulleys could hoist even the heaviest prizes up the rock face from a securely tied dinghy below.`
		},
		{
			group: ``,
			type: ``,
			audio: `alternativePayment`,
			phrase: `I employed a crew of particularly unsavory mariners, who, for a time, sailed the four corners at my behest - retrieving many valuable artifacts, relics and rare texts. Predictably, they increased their tariffs to counter my intense stipulations of secrecy. Such resources had long been exhausted, of course, and so I prepared an... alternative payment.`
		},
		{
			group: ``,
			type: ``,
			audio: `iHexedTheirAnchor`,
			phrase: `While the greedy dogs slept off their revelry, I hexed their anchor with every twisted incantation I could muster - imbuing it with the weight of my ambition, and my contempt for their crude extortion. At the witching hour, the anchor pulled with preternatural force, dragging craft and crew down into the depths. They must have cried out, but no sound escaped the swirling black waters...`
		},
	],
};