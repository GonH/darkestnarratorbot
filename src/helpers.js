const fs = require("fs");
const path = require("path");
const {
  phrases
} = require('./phrases');

const getRandomPhrase = phrasesArray => {
  if (phrasesArray.length === 0)
    return `Begone, fiend!`;

  const randomo = Math.floor(Math.random() * phrasesArray.length);
  const phrase = phrasesArray[randomo];

  return phrase;
};

const getAnyPhrase = () => {
  const anyPhrase = getRandomPhrase(phrases);

  return anyPhrase;
}

const getArguments = ({
  text
}) => {
  const [, ...rest] = text.split(" ");

  if (rest.length === 1) return rest;

  const input = rest.reduce((x, y) => {
    return x + " " + y;
  }, "");

  return input;
}

const getConstId = (text, dict) => {
  const id = dict[text];

  return id;
};

const getPhraseByArgument = (msg, attribute, attributeName) => {
  const attributeText = getArguments(msg);
  const attibuteId = getConstId(attributeText, attribute);

  const phrasesByAttribute = phrases.filter(phrase => {
    return phrase[attributeName] === attibuteId;
  })

  const phraseByAttribute = getRandomPhrase(phrasesByAttribute);
  const phrase = phraseByAttribute;

  return phrase;
};

const replyText = (msg, text) => {
  return msg.reply.text(text);
};

const replyAudio = async (msg, fileName) => {
  let fileToSend;
  try {
    const route = path.resolve(__dirname, `./assets/${fileName}.ogg`);
    fileToSend = await fs.readFileSync(route);
  } catch (e) {
    const route = path.resolve(__dirname, `./assets/begoneFiend.ogg`);
    fileToSend = await fs.readFileSync(route);
  }
  return msg.reply.audio(fileToSend);
};

const filterPhrases = (filter) => {
  const filteredPhrases = phrases.filter(phrase => {
    return phrase.phrase.includes(filter);
  });

  const textToReturn = getRandomPhrase(filteredPhrases);

  return textToReturn;
};

module.exports = {
  getRandomPhrase,
  getArguments,
  getAnyPhrase,
  getPhraseByArgument,
  replyText,
  replyAudio,
  filterPhrases
};