const {
	getAnyPhrase,
	getArguments,
	getPhraseByArgument,
	replyText,
	replyAudio,
	filterPhrases
} = require('./helpers');
const {
	group,
	type,
	situation
} = require('./constants');

const tellAnythingHandler = msg => {
	const {
		phrase
	} = getAnyPhrase();

	return replyText(msg, phrase);
};

const tellHandler = msg => {
	const input = getArguments(msg);
	const {
		phrase
	} = filterPhrases(input);

	return replyText(msg, phrase);
}

const tellByGroupHandler = msg => {
	const {
		phrase
	} = getPhraseByArgument(msg, group, "group");

	return replyText(msg, phrase);
}

const tellByTypeHandler = msg => {
	const {
		phrase
	} = getPhraseByArgument(msg, type, "type");

	return replyText(msg, phrase);
}

const tellBySituationHandler = msg => {
	const {
		phrase
	} = getPhraseByArgument(msg, situation, "situation");

	return replyText(msg, phrase);
}

const sayHandler = msg => {
	const input = getArguments(msg);
	const {
		audio
	} = filterPhrases(input);

	return replyAudio(msg, audio);
}

const sayByGroupHandler = msg => {
	const {
		audio
	} = getPhraseByArgument(msg, group, "group");

	return replyAudio(msg, audio);
}

const sayByTypeHandler = msg => {
	const {
		audio
	} = getPhraseByArgument(msg, type, "type");

	return replyAudio(msg, audio);
}

const sayBySituationHandler = msg => {
	const {
		audio
	} = getPhraseByArgument(msg, situation, "situation");

	return replyAudio(msg, audio);
}

const sayAnythingHandler = msg => {
	const {
		audio
	} = getAnyPhrase();

	return replyAudio(msg, audio);
}

module.exports = {
	tellAnythingHandler,
	tellHandler,
	tellByGroupHandler,
	tellByTypeHandler,
	tellBySituationHandler,
	sayHandler,
	sayByGroupHandler,
	sayByTypeHandler,
	sayBySituationHandler,
	sayAnythingHandler
};